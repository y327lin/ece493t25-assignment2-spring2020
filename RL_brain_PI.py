import numpy as np
import pandas as pd
import maze_env
from RL_brain_iter import rl_iter

class rlalgorithm(rl_iter):
    def __init__(self, env, delta=0.01, reward_decay=0.9):
        pd.set_option('display.max_rows', None)
        self.gamma = reward_decay
        self.env = env
        self.delta = delta
        self.display_name="Policy Iteration"
        self.build_v_table(env)
        self.rewards = []

        # Initial policy: Just go up
        self.action_table = pd.Series([])
        for s in self.v_table.index:
            s = list(s)
            self.action_table[str(s)] = 0

    def eval_state(self, s):
        a = self.action_table[str(s)]
        s_ = self.next_states(s)[a]
        val = self.compute_return(s, a, s_)
        diff = abs(val - self.v_table[tuple(s)])
        self.v_table[tuple(s)] = val
        return diff

    def eval_all(self):
        maxdiff = max([self.eval_state(list(s)) for s in self.v_table.index])
        self.rewards.append(self.v_table[(5.0, 5.0, 35.0, 35.0)])
        return maxdiff

    def pol_eval(self):
        maxdiff = self.eval_all()
        while maxdiff > self.delta:
            maxdiff = self.eval_all()

    def pol_improve(self):
        stable = True
        for s in self.v_table.index:
            s = list(s)
            old = self.action_table[str(s)]
            a, _ = self.choose_action_state(s)
            self.action_table[str(s)] = a
            if a != old: stable = False
        return stable

    def iterate(self):
        stable = False
        while not stable:
            self.pol_eval()
            stable = self.pol_improve()
