import numpy as np
import pandas as pd
import maze_env
from RL_brain_iter import rl_iter

class rlalgorithm(rl_iter):
    def __init__(self, env, delta=0.001, reward_decay=0.9):
        pd.set_option('display.max_rows', None)
        self.gamma = reward_decay
        self.env = env
        self.delta = delta
        self.display_name="Value Iteration"
        self.build_v_table(env)
        self.rewards = []

    def update(self, s):
        a, s_ = self.choose_action_state(s)
        val = self.compute_return(s, a, s_)
        diff = abs(val - self.v_table[tuple(s)])
        self.v_table[tuple(s)] = val
        return diff

    def update_all_states(self):
        maxdiff = max([self.update(list(s)) for s in self.v_table.index])
        self.rewards.append(self.v_table[(5.0, 5.0, 35.0, 35.0)])
        return maxdiff

    def iterate(self):
        maxdiff = self.update_all_states()
        while self.delta < maxdiff:
            maxdiff = self.update_all_states()
        
        self.action_table = pd.Series([])
        for s in self.v_table.index:
            s = list(s)
            a, _ = self.choose_action_state(s)
            self.action_table[str(s)] = a
