import numpy as np
import pandas as pd
import maze_env

# Base class for PI and VI algorithms
class rl_iter:
    def build_v_table(self, env):
        index = []
        for x in range(env.MAZE_W):
            for y in range(env.MAZE_H):
                center = maze_env.origin + np.array([x*env.UNIT, y*env.UNIT])
                state = [center[0] - 15.0, center[1] - 15.0, center[0] + 15.0, center[1] + 15.0]
                if state not in [env.canvas.coords(w) for w in env.pitblocks] and state not in [env.canvas.coords(w) for w in env.wallblocks]:
                    index.append(tuple(state))
        self.v_table = pd.Series(data=np.zeros(len(index)), index=index, dtype=np.float64)

    def choose_action(self, observation):
        # This is only called after iterate()
        return self.action_table[observation]

    def learn(self, s, a, r, s_):
        return s_, self.choose_action(s_)

    def compute_return(self, s, a, s_):
        if any([n < 0.0 or n > self.env.MAZE_W*self.env.UNIT for n in s_]): # check if we walk off the grid
            s_ = s
        reward, done, reverse = self.env.computeReward(s, a, s_)
        if reverse: s_ = s 

        if done: r = reward
        else: r = self.gamma * self.v_table[tuple(s_)] + reward
        return r

    def next_states(self, observation):
        new_states = [
            observation + np.array([0, -self.env.UNIT]*2), #up
            observation + np.array([0, self.env.UNIT]*2), #down
            observation + np.array([self.env.UNIT, 0]*2), #right
            observation + np.array([-self.env.UNIT, 0]*2), #left
        ]
        return [s.tolist() for s in new_states]

    def choose_action_state(self, observation):
        new_states = self.next_states(observation)

        a, s_ = max(enumerate(new_states), key=lambda pair: self.compute_return(observation, pair[0], pair[1]))
        return a, s_
